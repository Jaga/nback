# Simple grid-based N-back test
# where users click on where they
# believe the square to have been
# N changes ago
#
# author: Kyle Tanous

import pygame as pg
import sys
import random
import os

# params
N = 1
NUM_TRIALS = [21, 24, 29, 36][N-1]
GRID_SZ = 3
CELL_SZ = 100
TRIAL_TIME = 800
DELAY_TIME = 2200
GRID_CLR = (0, 0, 0)
BKG_CLR = (255, 255, 255)
CORR_CLR = (0, 255, 0)
INCORR_CLR = (255, 0, 0)
STIM_CLR = (120, 120, 155)
NUM_CORR = [10, 12, 14, 18][N-1]
TRIAL_SERIES = []
MARGIN = 5
TRIAL_STATE = 0 # 0 = default stim, 1 = correct guess, 2 = incorrect guess

pg.init()

# Draw empty grid
def draw_grid(size):
	screen.fill(BKG_CLR)
	sz = screen.get_size()
	CELL_SZ = sz[0] // GRID_SZ
	for i in range(GRID_SZ):
		pg.draw.line(screen, GRID_CLR, [i*CELL_SZ, 0], [i*CELL_SZ, sz[1]])
		pg.draw.line(screen, GRID_CLR, [0, i*CELL_SZ], [sz[0], i*CELL_SZ])


# Draw square at index in grid
def draw_stim(idx, flag):
	# Set color based on trial state
	if flag:
		clr = STIM_CLR
		if TRIAL_STATE == 1:
			clr = CORR_CLR
		elif TRIAL_STATE == 2:
			clr = INCORR_CLR

		r = idx // 3
		c = idx % 3
		pg.draw.rect(screen, clr, \
			pg.Rect(c*CELL_SZ+MARGIN, r*CELL_SZ+MARGIN, CELL_SZ-2*MARGIN, CELL_SZ-2*MARGIN))

# Generate match locations and build stimulus location array for entire session
def init_game(n):
	# Generate locations for matches and shuffle them (for each half of array)
	match_locations = [0 for _ in range(N)] + \
					  random.sample([1 for _ in range(NUM_CORR//2)] + [0 for _ in range((NUM_TRIALS-N)//2-NUM_CORR//2)], (NUM_TRIALS-N)//2) + \
					  random.sample([1 for _ in range(NUM_CORR//2)] + [0 for _ in range((NUM_TRIALS-N)//2-NUM_CORR//2)], (NUM_TRIALS-N)//2)

	# Build location array for stimulus
	for i in range(NUM_TRIALS):
		if match_locations[i]: 	# Create a match
			TRIAL_SERIES[i] = TRIAL_SERIES[i-N]
		else:					# Add random value (that doesn't result in N-back match)
			loc = random.randint(0, 8)
			while i >= N and loc == TRIAL_SERIES[i-N]: # Avoid superfluous matches
				loc = random.randint(0, 8)
			TRIAL_SERIES[i] = loc

if __name__ == '__main__':
	if len(sys.argv) < 3:
		print('Error: should have 2 arguments <N> and <log_file_name>\ne.g. python nback.py 2 participant_1')
		exit()

	# Check for Logs directory
	try:
		os.stat('./Logs')
	except:
		os.mkdir('./Logs')


	clock = pg.time.Clock()
	N = int(sys.argv[1])
	NUM_TRIALS = [21, 24, 29, 36][N-1]
	NUM_CORR = [10, 12, 14, 18][N-1]
	TRIAL_SERIES = [0 for _ in range(NUM_TRIALS)]
	log_file = open('Logs/' + sys.argv[2] + '.txt', 'w+')
	log_file.write('Time since session start (ms), Time since trial start (ms), Trial #, Correct/Incorrect, Match/No match\n')
	screen = pg.display.set_mode([300, 300])
	pg.display.set_caption(str(N) + '-back : ' + sys.argv[2])
	init_game(N)

	# Response type rates
	hit = 0
	miss = 0
	false_alarm = 0
	correct_rej = 0

	session_start = 0
	start_time = pg.time.get_ticks()
	trial = -1
	start = False
	while trial < NUM_TRIALS:
		clock.tick(60)
		events = pg.event.get()
		if not start: # Wait for space key press
			for event in events:
				if event.type == pg.KEYDOWN:
					if event.key == pg.K_SPACE:
						print('space pressed')
						start = True
						start_time = pg.time.get_ticks()
						session_start = start_time
			draw_grid(GRID_SZ)
		else: # Test has started, start presenting timed trials
			curr_time = pg.time.get_ticks()

			# Change stimulus location after duration TRIAL_TIME
			if trial < 0 or curr_time - start_time > TRIAL_TIME + DELAY_TIME:
				# Check if user didn't click, and whether that was correct response
				if TRIAL_STATE == 0 and trial >= 0:
					if trial < N or TRIAL_SERIES[trial] != TRIAL_SERIES[trial - N]: # didn't click because no match; correct
						log_file.write(str(curr_time - session_start) + ', ' + \
							str(curr_time - start_time) + ', ' + str(trial+1) + ', C, N\n')
						correct_rej += 1
					else:											 # didn't click and missed match; incorrect
						log_file.write(str(curr_time - session_start) + ', ' + \
							str(curr_time - start_time) + ', ' + str(trial+1) + ', I, M\n')
						miss += 1

				trial += 1
				start_time = curr_time
				TRIAL_STATE = 0
				if trial >= NUM_TRIALS:
					break

			stim_flag = False
			if curr_time - start_time <= TRIAL_TIME:
				pg.event.set_allowed(pg.MOUSEBUTTONUP)
				stim_flag = True
			elif curr_time - start_time <= TRIAL_TIME + DELAY_TIME:
				pg.event.set_blocked(pg.MOUSEBUTTONUP)
				stim_flag = False

			draw_grid(GRID_SZ)
			draw_stim(TRIAL_SERIES[trial], stim_flag)


			# Check for mouse clicks
			for event in events:
				if event.type == pg.MOUSEBUTTONUP:
					x, y = pg.mouse.get_pos()
					cell = x // CELL_SZ + 3 * (y // CELL_SZ)

					# Check for correctness of click
					if TRIAL_STATE == 0:
						if trial < N or cell != TRIAL_SERIES[trial - N]:
							TRIAL_STATE = 2
							log_file.write(str(curr_time - session_start) + ', ' + \
								str(curr_time - start_time) + ', ' + str(trial+1) + ', I, N\n')
							false_alarm += 1
						else:
							TRIAL_STATE = 1
							log_file.write(str(curr_time - session_start) + ', ' + \
								str(curr_time - start_time) + ', ' + str(trial+1) + ', C, M\n')
							hit += 1

		pg.display.update()
	
	log_file.write('Hits: ' + str(hit) + ', Misses: ' + str(miss) + \
		', False Alarms: ' + str(false_alarm) + ', Correct Rejections: ' + str(correct_rej) + '\n')
	log_file.write('Score: ' + str(hit) + '/' + str(NUM_CORR) + '\n')
	log_file.close()
	exit()
